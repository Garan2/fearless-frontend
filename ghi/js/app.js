function createCard(name, description, pictureUrl, dates, location) {
  return `
    <div class="card shadow p-3 mb-5 bg-body-rounded">
      <img src="${pictureUrl}" class="card-img-top">
      <div class="card-body">
        <h5 class="card-title">${name}</h5>
        <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
        <p class="card-text">${description}</p>
        <div class="card-footer">
    ${dates} </div>
      </div>
    </div>
  `
}
window.addEventListener('DOMContentLoaded', async () => {

  const url = 'http://localhost:8000/api/conferences/';

  try {
    const response = await fetch(url);

    if (!response.ok) {
      // Figure out what to do when the response is bad
      throw new Error('Response not ok');
    } else {
      const data = await response.json();

      for (let [index, conference] of data.conferences.entries()) {
        const detailUrl = `http://localhost:8000${conference.href}`;
        const detailResponse = await fetch(detailUrl);
        let indexstr = index.toString()
        if (detailResponse.ok) {
          const details = await detailResponse.json();
          console.log(details)

          const name = details.conference.name;
          const description = details.conference.description;
          const pictureUrl = details.conference.location.picture_url;
          const start_date = new Date(details.conference.starts).toLocaleDateString()
          const ends_date = new Date(details.conference.ends).toLocaleDateString()
          const dates = start_date + '-' + ends_date
          const location = details.conference.location.name;
          const html = createCard(name, description, pictureUrl, dates, location);
          const column = document.querySelector('.spec' + indexstr);
          column.innerHTML += html;
        }
      }
    }
  } catch (e) {
    // Figure out what to do if an error is raised
    console.error('error', e)
  }

});
